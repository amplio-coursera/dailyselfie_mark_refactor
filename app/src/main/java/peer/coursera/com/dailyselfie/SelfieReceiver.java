package peer.coursera.com.dailyselfie;

import android.app.Notification;
import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;

/**
 * Created by Mark on 4/30/2015.
 */
public class SelfieReceiver extends BroadcastReceiver {

    private final int MY_NOTIFICATION_ID = 11151990;
    private final CharSequence tickerText = "Have you Selfie'd";
    private final CharSequence contentTitle = "Selfie Reminder";
    private final CharSequence contentText = "Go take a selfie, one is never enough";

    // Notification Action Elements
    private Intent mNotificationIntent;
    private PendingIntent mContentIntent;





    @Override
    public void onReceive(Context context, Intent intent) {

        mNotificationIntent = new Intent(context, DailySelfie.class);

        // The PendingIntent that wraps the underlying Intent
        mContentIntent = PendingIntent.getActivity(context, 0,
                mNotificationIntent, PendingIntent.FLAG_UPDATE_CURRENT);


        // Build the Notification
        Notification.Builder notificationBuilder = new Notification.Builder(
                context).setTicker(tickerText)
                .setSmallIcon(android.R.drawable.ic_menu_camera)
                .setAutoCancel(true).setContentTitle(contentTitle)
                .setContentText(contentText).setContentIntent(mContentIntent);
               // .setSound(soundURI).setVibrate(mVibratePattern);

        // Get the NotificationManager
        NotificationManager mNotificationManager = (NotificationManager) context
                .getSystemService(Context.NOTIFICATION_SERVICE);

        // Pass the Notification to the NotificationManager:
        mNotificationManager.notify(MY_NOTIFICATION_ID,
                notificationBuilder.build());

    }
}
