package peer.coursera.com.dailyselfie;

import android.graphics.Bitmap;
import android.net.Uri;

/**
 * Created by Mark on 5/24/2015.
 */
public class Selfie {
    private String mSelfieUrl;
    private Uri mSelfieUri;
    private String mPictureName;
    private Bitmap mSelfieBitmap;
    private final int Thumbnail_WIDTH = 40;
    private final int Thumbnail_HEIGHT = 40;
    public final static String URL = "url";
    public final static String PIC = "pic";
    public final static String BITMAP = "bitmap";

    public Selfie(String selfieUrl, String picture, Bitmap selfieBitmap) {
        this.mSelfieUrl = selfieUrl;
        this.mPictureName = picture;
        this.mSelfieBitmap = selfieBitmap;

    }

    public Selfie(Uri selfieUri, String picture) {
        this.mSelfieUri = selfieUri;
        this.mPictureName = picture;
    }
/*
    Selfie (Intent intent) {

        mSelfieUrl = intent.getStringExtra(Selfie.URL);
        mPictureName = intent.getStringExtra(Selfie.PIC);
        mSelfieBitmap = intent.getE(Selfie.BITMAP, getCountSet());

    }
*/
    public Selfie() {
    }

    public String getSelfieUrl() {
        return mSelfieUrl;
    }

    public void setSelfieUrl(String selfieUrl) {
        this.mSelfieUrl = selfieUrl;
    }

    public String getPictureName() {
        return mPictureName;
    }

    public void setPictureName(String picture) {
        this.mPictureName = picture;
    }



    public Bitmap getSelfieBitmap() {
        return mSelfieBitmap;
    }

    public void setSelfieBitmap(Bitmap SelfieBitmap) {
        this.mSelfieBitmap = SelfieBitmap;
    }





    @Override
    public String toString(){
        return "File: " + mPictureName;

    }
}
