package peer.coursera.com.dailyselfie;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.io.File;
import java.util.ArrayList;
import java.util.List;

/**
 * Created by Mark on 5/24/2015.
 */
public class SelfieAdapter extends BaseAdapter {

    private List<Selfie> list = new ArrayList<Selfie>();
    private static LayoutInflater inflater = null;
    private Context mContext;

    public SelfieAdapter(Context context, List<Selfie> items ) {
        mContext = context;
        inflater = (LayoutInflater) mContext
                .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        this.list = items;
    }

    public int getCount() {
        return list.size();
    }

    public Object getItem(int position) {
        return list.get(position);
    }

    public long getItemId(int position) {
        return position;
    }

    public View getView(int position, View convertView, ViewGroup parent) {

        View newView = convertView;
        ViewHolder holder;

        Selfie curr = list.get(position);

        if (null == convertView) {
            holder = new ViewHolder();
            newView = inflater
                    .inflate(R.layout.selfie_item, parent, false);
            holder.selfieimage = (ImageView) newView.findViewById(R.id.selfieImage);
            holder.selfiename = (TextView) newView.findViewById(R.id.selfie_name);
            //holder.place = (TextView) newView.findViewById(R.id.place_name);
            newView.setTag(holder);

        } else {
            holder = (ViewHolder) newView.getTag();
        }

        holder.selfieimage.setImageBitmap(curr.getSelfieBitmap());
        holder.selfiename.setText("File: " + curr.getPictureName());
       // holder.place.setText("Place: " + curr.getPlace());

        return newView;
    }

    static class ViewHolder {

        ImageView selfieimage;
        TextView selfiename;
        TextView place;

    }

    public void add (Selfie listItem) {
        list.add(listItem);
        notifyDataSetChanged();
    }


    public  List<Selfie> getList() {
        return list;
    }

    public void removeAllViews() {
        list.clear();
        this.notifyDataSetChanged();
    }

    public ArrayList<String> GetFiles(String DirPath){
        ArrayList<String> SelfieFiles = new ArrayList<String>();
        File f = new File(DirPath);

        f.mkdirs();
        File[] files = f.listFiles();
        if(files.length == 0){
            return null;
        }
        else {
            for (int i=0; i< files.length ; i++)
            {
                SelfieFiles.add(files[i].getName());
            }
        }

        return SelfieFiles;
    }
}
