package peer.coursera.com.dailyselfie;

import android.app.AlarmManager;
import android.app.PendingIntent;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.ThumbnailUtils;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.ActionBarActivity;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.ListView;
import android.widget.Toast;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class DailySelfie extends ActionBarActivity {


    ArrayList<String> FilesInFolder;
    ArrayList<Selfie> selfies;
    SelfieAdapter mAdapter;
    private AlarmManager selfAlarm;
    final private int ALARM_DELAY = 2 * 60 * 1000;
    static final int REQUEST_IMAGE_CAPTURE = 1;
    private ImageView mImageView;
    private String mCurrentPhotoPath;
    Button btTakePic;
    String picDirPath = //"file:/" +
            Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES).toString() + "/" ;
    private final int Thumbnail_WIDTH = 15;
    private final int Thumbnail_HEIGHT = 10;
    private static final String FILE_NAME = "SelfieList.txt";


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_daily_selfie);
        selfAlarm = (AlarmManager) getSystemService(Context.ALARM_SERVICE);
        setRepeatingAlarm();
        ListView lv = (ListView) findViewById(R.id.selfieList);

        Log.i("log message", Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString());


        FilesInFolder = null;

        if(GetFiles(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString()) != null) {
         FilesInFolder = GetFiles(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString());
            CreateSelfieList(FilesInFolder);
        }




        if (FilesInFolder != null) {

            mAdapter = new SelfieAdapter(getApplicationContext(), selfies);
            lv.setAdapter(mAdapter);

       lv.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position, long id) {

                    String filePath = "file:/" +picDirPath + FilesInFolder.get(position);
                    Uri fileUri = Uri.parse(filePath);
                    Intent intent = new Intent();
                    intent.setAction(Intent.ACTION_VIEW);
                    intent.setDataAndType(fileUri, "image/*");
                    startActivity(intent);
                }
            });
        }

    }

    @Override
    protected void onResume() {
        super.onResume();

        /*
        ArrayList<String> files;
        int numFiles;
        files = GetFiles(picDirPath);
        mAdapter.removeAllViews();

        numFiles = files.size();

        File f;
        Uri contentUri;
        Bitmap bm = null, thumb = null;
        Selfie listSelfie;



        for (int i = 0; i<numFiles; i++){

            f = new File(files.get(i));
            contentUri = Uri.fromFile(f);
            try {
                bm = BitmapFactory.decodeStream(
                        getContentResolver().openInputStream(contentUri));
                thumb = ThumbnailUtils.extractThumbnail(bm, Thumbnail_HEIGHT, Thumbnail_WIDTH);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            listSelfie = new Selfie(files.get(i), files.get(i), thumb);
            mAdapter.add(listSelfie);
        }




        if (selfies!=null)
        {
            CreateSelfieList();
            mAdapter = new SelfieAdapter(getApplicationContext(), selfies);
            setListAdapter(mAdapter);
            mAdapter.notifyDataSetChanged();
        }

        */



    }

    public void setRepeatingAlarm() {
        Intent intent = new Intent(this, SelfieReceiver.class);
        PendingIntent pendingIntent = PendingIntent.getBroadcast(this, 0,
                intent, PendingIntent.FLAG_CANCEL_CURRENT);
        selfAlarm.setRepeating(AlarmManager.RTC_WAKEUP, System.currentTimeMillis(),
                ALARM_DELAY, pendingIntent);
    }

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_daily_selfie, menu);
        return true;
    }



    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        switch (item.getItemId()) {
            case R.id.action_settings:
                return true;
            case R.id.camera_icon:

                startCameraCapture();

                //break;
                return true;
            default:
                return super.onOptionsItemSelected(item);
        }

    }

    public void startCameraCapture() {

        Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);

        if (intent.resolveActivity(getPackageManager()) == null)
        {
            Log.i("startcamcapture", "Cannot Resolve activity");
        }


        // Ensure that there's a camera activity to handle the intent
        if (intent.resolveActivity(getPackageManager()) != null) {
            // Create the File where the photo should go
            File photoFile = null;
            try {
                photoFile = createImageFile();
            } catch (IOException ex) {
                // Error occurred while creating the File
                ex.printStackTrace();

            }
            // Continue only if the File was successfully created
            if (photoFile != null) {
                intent.putExtra(MediaStore.EXTRA_OUTPUT,
                        Uri.fromFile(photoFile));
                startActivityForResult(intent, REQUEST_IMAGE_CAPTURE);
            }

        }
    }

    private File createImageFile() throws IOException {
        // Create an image file name
        String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
        String imageFileName = "JPEG_" + timeStamp + "_";
        File storageDir = Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES);
        File image = File.createTempFile(
                imageFileName,  /* prefix */
                ".jpg",         /* suffix */
                storageDir      /* directory */
        );

        // Save a file: path for use with ACTION_VIEW intents
        mCurrentPhotoPath = "file:" + image.getAbsolutePath();
        return image;
    }




    protected void onActivityResult(int requestCode, int resultCode, Intent data) {

        File f = new File(mCurrentPhotoPath);
        Uri contentUri = Uri.fromFile(f);
        Bitmap bm = null, thumb = null;
        ArrayList<String> fn;
        int fileCount;
        fn = GetFiles(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES).toString());

        fileCount = fn.size();




        if (requestCode == REQUEST_IMAGE_CAPTURE && resultCode == RESULT_OK) {

            try {
                bm = BitmapFactory.decodeStream(
                        getContentResolver().openInputStream(contentUri));
                thumb = ThumbnailUtils.extractThumbnail(bm, Thumbnail_HEIGHT, Thumbnail_WIDTH);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }

            Intent mediaScanIntent = new Intent (Intent.ACTION_MEDIA_SCANNER_SCAN_FILE);
            this.sendBroadcast(mediaScanIntent);

            //Bundle extras = data.getExtras();
            //Bitmap imageBitmap = (Bitmap) extras.get("data");
            //mImageView.setImageBitmap(imageBitmap);
            //galleryAddPic();
            String test = fn.get(fileCount-1).toString();
            Selfie td = new Selfie(fn.get(fileCount-1).toString(), fn.get(fileCount-1).toString(), thumb);

            mAdapter.add(td);
            mAdapter.notifyDataSetChanged();


        }
        else
        {
            Toast.makeText(DailySelfie.this, "The picture no take ", Toast.LENGTH_SHORT)
                    .show();
        }


    }

    public ArrayList<String> GetFiles(String DirPath){
        ArrayList<String> SelfieFiles = new ArrayList<String>();
        File f = new File(DirPath);

        f.mkdirs();
        File[] files = f.listFiles();
        if(files.length == 0){
            return null;
        }
        else {
            for (int i=0; i< files.length ; i++)
            {
                SelfieFiles.add(files[i].getName());
            }
        }

        return SelfieFiles;
    }

    public void CreateSelfieList(ArrayList<String> filesInFolder) {

        selfies = null;
        selfies = new ArrayList<Selfie>();
        int numFiles;
        String fileName;
        //mAdapter.removeAllViews();

        numFiles = filesInFolder.size();

        File f;
        Uri contentUri;
        Bitmap bm = null, thumb = null;
        Selfie listSelfie;



        for (int i = 0; i<numFiles; i++){

            fileName = filesInFolder.get(i).toString();
            f = new File(picDirPath + fileName);
            contentUri = Uri.fromFile(f);
            try {
                bm = BitmapFactory.decodeStream(
                        getContentResolver().openInputStream(contentUri));
                thumb = ThumbnailUtils.extractThumbnail(bm, Thumbnail_HEIGHT, Thumbnail_WIDTH);
            } catch (FileNotFoundException e) {
                e.printStackTrace();
            }
            new Selfie(fileName, fileName, thumb);
            listSelfie = new Selfie(fileName, filesInFolder.get(i).toString(), thumb);
            selfies.add(listSelfie);
        }

    }
}


